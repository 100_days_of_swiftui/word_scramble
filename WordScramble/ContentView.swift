//
//  ContentView.swift
//  WordScramble
//
//  Created by Hariharan S on 11/05/24.
//

import SwiftUI

struct ContentView: View {
    
    // MARK: - State Properties

    @State private var newWord = ""
    @State private var rootWord = ""
    @State private var usedWords = [String]()
    
    @State private var errorTitle = ""
    @State private var errorMessage = ""
    @State private var showingError = false
    @State private var allWords: [String] = []
    
    var body: some View {
        NavigationStack {
            List {
                Section {
                    TextField(
                        "Enter your word",
                        text: self.$newWord
                    )
                    .font(.headline)
                    .textInputAutocapitalization(.never)
                }
                
                if !self.usedWords.isEmpty {
                    Section("Founded Anagrams") {
                        ForEach(
                            self.usedWords,
                            id: \.self
                        ) { word in
                            HStack {
                                Image(systemName: "\(word.count).circle")
                                Text(word)
                            }
                        }
                    }
                }
            }
            .onSubmit {
                self.addNewWord()
            }
            .onAppear {
                self.loadStringsFromFile()
            }
            .toolbar {
                ToolbarItem(placement: .topBarLeading) {
                    Button {
                        self.startGame()
                    } label: {
                        Image(systemName: "arrow.clockwise")
                    }
                }
                
                ToolbarItem(placement: .topBarTrailing) {
                    Text("Score: \(self.usedWords.count)")
                }
            }
            .alert(
                self.errorTitle,
                isPresented: $showingError
            ) { } message: {
                Text(self.errorMessage)
            }
            .navigationTitle(self.rootWord)
        }
    }
}

// MARK: - Private Methods

private extension ContentView {
    func addNewWord() {
        let answer = self.newWord.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        guard answer.count > 0 
        else {
            return
        }
        
        guard self.isOriginal(word: answer)
        else {
            self.wordError(
                title: "Word used already",
                message: "Be more original"
            )
            return
        }

        guard self.isPossible(word: answer)
        else {
            self.wordError(
                title: "Word not possible",
                message: "You can't spell that word from '\(self.rootWord)'!"
            )
            return
        }

        guard self.isReal(word: answer)
        else {
            self.wordError(
                title: "Word not recognized",
                message: "You can't just make them up, you know!"
            )
            return
        }
        
        withAnimation {
            usedWords.insert(answer, at: 0)
        }
        self.newWord = ""
    }
    
    func loadStringsFromFile() {
        guard let startWordsURL = Bundle.main.url(
            forResource: "Words",
            withExtension: "txt"
        ) else {
            return
        }
        
        if let startWords = try? String(contentsOf: startWordsURL) {
            self.allWords = startWords.components(separatedBy: "\n")
            self.startGame()
            return
        }
    }
    
    func startGame() {
        self.usedWords = []
        self.rootWord = self.allWords.randomElement() ?? "silkworms"
    }

    func isOriginal(word: String) -> Bool {
        !self.usedWords.contains(word)
    }
    
    func isPossible(word: String) -> Bool {
        var tempWord = self.rootWord
        for letter in word {
            if let pos = tempWord.firstIndex(of: letter) {
                tempWord.remove(at: pos)
            } else {
                return false
            }
        }
        return true
    }
    
    func isReal(word: String) -> Bool {
        guard word.count > 1
        else {
            return false
        }
        
        let checker = UITextChecker()
        let range = NSRange(location: 0, length: word.utf16.count)
        let misspelledRange = checker.rangeOfMisspelledWord(
            in: word,
            range: range,
            startingAt: 0,
            wrap: false,
            language: "en"
        )
        return misspelledRange.location == NSNotFound
    }
    
    func wordError(title: String, message: String) {
        self.errorTitle = title
        self.errorMessage = message
        self.showingError = true
    }
}

#Preview {
    ContentView()
}
