//
//  WordScrambleApp.swift
//  WordScramble
//
//  Created by Hariharan S on 11/05/24.
//

import SwiftUI

@main
struct WordScrambleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
